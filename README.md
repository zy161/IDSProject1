# IDS721 Project 1

## The Link to My Demo Video
- [**Check My Demo Video**](https://youtu.be/PfgQI6Ta4GU)

## The AWS S3 Link to My Website
- [**Visit My Webpage on AWS S3 Bucket**](http://idsproject1.s3-website-us-east-1.amazonaws.com/)

## Create Zola Webpage
Follow the Week1's guide to create and build a Zola weopage.
1. Install Zola: install Zola using Chocolate on Windows Powershell. 

2. Project Initialization: After installation, navigate to our project directory in the terminal and run `zola init` to initialize a new Zola project.

3. Update the website content and theme: Browse the Zola themes directory to find a theme that suits your website's aesthetics and functionality needs. Each theme comes with its own set of features and design elements, clone the template's repository into your themes directory within your Zola project. Update the `content` folder for our own website contents.

4. Running the Zola Server: To view the site locally as you develop, run `zola serve` within the project directory. This command starts a local server, usually accessible at http://127.0.0.1:1024, allowing you to preview the site in real-time.

![Local Preview](img/localserve.png)

## The Structure of My Website
```plaintext
├─content
│  ├─about
│  ├─blog
│  │  └─blog-with-image
│  ├─projects
│  │  ├─project-1
│  │  ├─project-2
│  │  └─project-3
│  └─workflows
├─pics
├─sass
├─static
│  └─processed_images
└─templates
   ├─categories
   ├─macros
   ├─shortcodes
   └─tags
```

## Host the Webpage on AWS S3 Bucket with Gitlab CI
Deploying the Zola site to GitLab requires setting up a CI/CD pipeline. To host the webpage on AWS S3 bucket needs hosting and deployment configuration.

To allow hosting a static page on S3, we need to follow these instructions:

### Configure AWS S3 Bucket and Permissions
To host a static webpage, we need to first create an S3 bucket on AWS.
1. Create a new S3 bucket called `idsproject1`, disabling `Block all public access` and enabling `Static website hosting`

![Block Permission](img/blockall.png)

2. Create a new bucket policy to allow public access

![Bucket Policy](img/bucketpolicy.png)
### Grant IAM User with Proper Permissions
To allow Gitlab pushing files to S3 bucket, we need to configure IAM user's permission policy and Gitlab project variable.
1. Create new permission policy in IAM -> Policies:
```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AccessToWebsiteBuckets",
            "Effect": "Allow",
            "Action": [
                "s3:PutBucketWebsite",
                "s3:PutObject",
                "s3:PutObjectAcl",
                "s3:GetObject",
                "s3:ListBucket",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::idsproject1",
                "arn:aws:s3:::idsproject1/*"
            ]
        },
        {
            "Sid": "AccessToCloudfront",
            "Effect": "Allow",
            "Action": [
                "cloudfront:GetInvalidation",
                "cloudfront:CreateInvalidation"
            ],
            "Resource": "*"
        }
    ]
}
```
2. Attach this policy to the user who's in charge of hosting under Users
3. Generate a new access keys under the user's security credentials, save the access key and secret access key

![Access Key](img/accesskeys.png)

4. In the project's Gitlab repository, go to Settings -> CI/CD -> Variables, create new variables with information from AWS
   
![CI Variables](img/civariables.png)   

### Create .gitlab-ci.yml File to Create Workflow
Create a yml file called `.gitlab-ci.yml` in the project:
```yaml
stages:
  - build
  - deploy

image: alpine:latest

variables:
  # set to "recursive".
  GIT_SUBMODULE_STRATEGY: "recursive"
  S3_BUCKET_NAME: $S3_BUCKET_NAME
  AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
  AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
  AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION

build:
  stage: build
  script:
    - apk add --update-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community/ zola
    - zola build
  artifacts:
    paths:
      - public
  only:
    - main

deploy:
  stage: deploy
  script:
    - apk add --no-cache python3 py3-pip python3-dev build-base libffi-dev
    - python3 -m venv awscli-venv
    - source awscli-venv/bin/activate
    - pip install awscli
    - aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID
    - aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY
    - aws configure set default.region $AWS_DEFAULT_REGION
    - aws s3 sync public s3://$S3_BUCKET_NAME/ --delete
    - deactivate
  dependencies:
    - build
  rules:
    - changes:
        - content/**/*
        - images/**/*
    - if: '$CI_COMMIT_BRANCH == "main"'
```

This configuration build the zola site and deploy the site to AWS S3 bucket.

### Push and Deploy the Webpage Hosted on AWS S3 Bucket
As soon as we push the `.yml` file to Gitlab repository, the CI/CD will automatically detect the file and run the pipeline.