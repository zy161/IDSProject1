+++
title = "Relative Path of Image"
date = 2024-01-21

[taxonomies]
tags = ["other"]
+++

Use `img()` to specify the relative path of image.

<!-- more -->

{{ img(path="./water.jpg", alt="") }}