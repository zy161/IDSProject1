+++
title = "About"
render = true
template = "about.html"
+++

{{ img(path="@/about/me.jpg", class="bordered", alt="It's me!", caption="A random photo of water I found") }}

&nbsp;

Hi! This is a website for showcasing Zeyu Yuan's mini-projects of IDS 721 2024 Spring.